# F# .NET Core using Serilog Logger

## Overview

[Serilog](https://serilog.net/) is an open source library, [Apache 2.0](https://github.com/serilog/serilog/blob/dev/LICENSE) license, to write structured log messages to a variety of log sinks. This simple example uses F# to create loggers that write JSON formatted messages to both the console and to a log file.

It demonstrates using the full and compact JSON formatting. Text formatters are passed as the first argument when building a `Sink` (here, `File` and `Console`). Log message enrichment with additional properties is also shown. A couple of the log statements are enriched with the scope of the enrichment controlled by the `using` function.

## Run it

    dotnet run
