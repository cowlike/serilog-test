﻿open System
open Serilog
open Serilog.Exceptions
open Serilog.Formatting.Compact
open Serilog.Formatting.Json
open Serilog.Context

type TestRecord = { stringField: string; intField: int }

let logInit () =
    Log.Logger <- 
        LoggerConfiguration()
            .Enrich.FromLogContext()
            .Enrich.WithExceptionDetails()
            .MinimumLevel.Information()
            .WriteTo.Console(CompactJsonFormatter())
            .WriteTo.File(
                JsonFormatter(), 
                "log.log", 
                rollingInterval = RollingInterval.Day, 
                rollOnFileSizeLimit = true, 
                retainedFileCountLimit = Nullable(7))
            .CreateLogger()

type HighLevelException(message:string, innerException:Exception) =
    inherit Exception(message, innerException)

[<EntryPoint>]
let main argv =
    logInit ()

    using (LogContext.PushProperty("A", 1)) (fun _ -> 
        Log.Information("Hello, Serilog!")
        Log.Information("list: {@MyList}", [1..5]))

    Log.Information("Instance {Count} = {@MyRecord}", 199, { stringField = "abc"; intField = 99 })
    Log.Information("arr: {@StringArray}", "abc,def".Split([|','|]))

    // Log with an innerException    
    try
        try 1 / 0 |> ignore
        with e -> raise <| HighLevelException("oops", e)
    with e -> Log.Error(e, "oops")

    Log.CloseAndFlush();
    0
